# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#############################################################################################################################################################################
#                                                                                                                                                                           #
#  FUNCTION DIRECTLY RELATED TO THE WIMES APPLICATION                                                                                                                       #
#                                                                                                                                                                           #
#############################################################################################################################################################################

#' Log a message in WIMES application
#'
#' @param message String - Message to be logged
Local_Log <- function(message){
  #cat(message, file="log.txt", sep='\n', append="TRUE")
  print(message)
}

#' log a warning message in WIMES application
#'
#' @param message String - Message to be logged
Local_Warning <- function(message){
  Local_Log("==== WARNING MESSAGE")
  Local_Log(message)
  #cat(message, file="warning.txt", sep='\n', append="TRUE")
  #print(message)
}

#' Log an error message in WIMES application
#'
#' @param message String - Message to be logged
Local_Error <- function(message){
  Local_Log("")
  Local_Log("==========================")
  Local_Log("/!\\ END WITH FATAL ERROR  ")
  Local_Log("==========================")
  Local_Log("")
  Local_Log(message)
  #cat(message, file="error.txt", sep='\n', append="TRUE")
  print(message)
}


#' Initialize the log function
#'
Local_logs_initialisation=function(){
  #cat("", file="error.txt", sep='\n')
  #cat("", file="warning.txt", sep='\n')
  #cat("", file="log.txt", sep='\n')
}


#' Create or update a quantitative value
#'
#' @param code String - Time series code
#' @param date Date - Observation date time
#' @param value Double - Value
#' @param log Function - log function
LocalUpsertQuantValue=function(code, date, value, mylog)
{
  tryCatch(
    {
      mylog(paste(
        "Upsert QuantValue [",
        date,
        ", ",
        value,
        "] into ",
        code,
        " time series",
        sep = ""))
      
      upsertQuantValue(code,
                       date,
                       value)
    },
    error = function(e){
      mylog(e,2)
    })
}


#' Create or update a quantitative forecast value
#'
#' @param code String - Time series code
#' @param productionDate Date - Production date time
#' @param date Date - Forecast date time
#' @param value Double - Value
#' @param log Function - log function
LocalUpsertQuantForecastValue=function(code, productionDate, date, value, mylog)
{
  tryCatch(
    {
      mylog(paste(
        "Upsert QuantForecastValue [",
        productionDate,
        ", ",
        date,
        ", ",
        value,
        "] into ",
        code,
        " time series",
        sep = ""))
      
      upsertQuantForecastValue(code,
                               productionDate,
                               date,
                               value)
    },    error = function(e){
      mylog(e,2)}
  )
}


#' Create or update a qualitative value
#'
#' @param code String - Time series code
#' @param date Date - Observation date time
#' @param value String - Value
#' @param log Function - log function
LocalUpsertQualValue=function(code, date, value, mylog)
{
  tryCatch({
    mylog(paste(
      "Upsert QualValue [",
      date,
      ", ",
      value,
      "] into ",
      code,
      " time series",
      sep = ""))
    
    upsertQualValue(code,
                    date,
                    value)
  },
  error = function(e){
    mylog(e,2)}
  )
}


###########################################################################################################################################
#                                                                                                                                         #
###########################################################################################################################################

sinusoide = function(t, periode, dephasage)
{
  val = 2*pi/periode * t + dephasage
  return((cos(val)+1)/2)
}


forecastCalculation = function(t, initial_date, periode, ratio, amplitude, base,  with_alea){
  #tryCatch(
  #  {
  dt = as.numeric(difftime(t, initial_date, units="hours"))
  q = amplitude * sinusoide(dt, periode,  ratio*2*pi) 
  if (with_alea){
    alea = amplitude * sample(-10:10,1)/100
    q = max(q + alea, base)
  }
  return(q)
  # }, 
  #  error = function(e){
  ###    print(error)
  ##    return(NA)
  # }
  #)
}


###########################################################################################################################################
# GENERATEUR POUR LA PAGE FLUVIALE                                                                                                        #
###########################################################################################################################################

#' @export
riverFloodingGenerator = function()
{
  
  # LOG INITIALIZATION
  mylog = function(message, level = 1){
    if (level == 1){
      Local_Log(message)
    } else if (level == 2) {
      Local_Warning(message)
    } else if (level == 3) {
      Local_Error(message)
    } else {
      Local_Warning( "Level log unknown :" + level)
      Local_Warning(message)
    }
  }
  Local_logs_initialisation()
  
  # CONSTANTS
  initial_date = as.POSIXct("2019-04-16T00:00:00Z")
  now = as.POSIXct(Sys.time(),"GMT")
  now = as.POSIXct(as.Date(now))
  
  # ###################################################################################################################################
  # CAN BE MODIFIED
  # ###################################################################################################################################
  
  forecast_duration_in_hours = 10 * 24 # forecast depth 
  forecast_timestep_in_hours = 1 * 24  # forecast timestep (1 forecast each forecast_timestep_in_hours)
  foreacast_periode_in_hours = 24 * 24 # the forecast is the same each foreacast_periode_in_hours
  Qmax = c(75.2, 127.3, 156.3, 200)
  Qbase = c(5.8, 11.6, 17.4, 20)
  Rmax = c(100, 100, 100, 100)
  
  alerts_names=c("green", "yellow", "orange", "red")
  alerts_limits=c(0.25,0.75,0.9,1000)
  
  stations = c("virt01", "virt02", "virt03", "virt04")
  reaches = c("riv01", "riv02", "riv03", "riv04", "riv05", "riv06", "riv07")
  
  QobsCodes = paste(stations, "Qobs", sep="")
  PobsCodes = paste(stations, "Pobs", sep="")
  QmaxCodes = paste(stations, "Qmax", sep="")
  QprevCodes = paste(stations, "Qprev", sep="")
  PprevCodes = paste(stations, "Pprev", sep="")
  alertCodes = paste(stations, "alert", sep="")
  rivAlertCodes = paste(reaches, "alert", sep="")
  
  rivLimits = list(c("virt02", "virt03"), c("virt01"), c("virt04"), c("virt04"), c("virt01"),c("virt02"),c("virt03"))
  
  # ###################################################################################################################################
  
  # variables initilization
  forecast_dates = c()
  flow_forecast_values = c()
  rain_forecast_values = c()
  alert_forecast_values = c()
  flow_observed_values = c()
  rain_observed_values = c()
  forecast_number = forecast_duration_in_hours / forecast_timestep_in_hours
  stations_number = length(stations)
  
  # loop over all forecast time
  for (i in 1:forecast_number){
    
    # forecast date
    t = now + (i-1) * forecast_timestep_in_hours * 3600
    forecast_dates = c(forecast_dates, t)
    
    # forecast values
    for (j in 1:stations_number){
      # flow forecast
      q = forecastCalculation(t, initial_date, foreacast_periode_in_hours, j/stations_number, Qmax[j], Qbase[j],  TRUE)
      flow_forecast_values = c(flow_forecast_values, q)
      
      # rainfall forecast
      p = forecastCalculation(t, initial_date, foreacast_periode_in_hours, j/stations_number, Rmax, 0,  FALSE)
      rain_forecast_values = c(rain_forecast_values, p)
    }
  }
  forecast_dates = as.POSIXct(forecast_dates, origin="1970-01-01")
  
  # observations
  observed_date = as.POSIXct(now - 86400) 
  for (j in 1:stations_number){
    # flow calculation
    q = forecastCalculation(observed_date, initial_date, foreacast_periode_in_hours, j/stations_number, Qmax[j], Qbase[j],  TRUE)
    flow_observed_values = c(flow_observed_values, q)
    # rainfall calculation
    p = forecastCalculation(observed_date, initial_date, foreacast_periode_in_hours, j/stations_number, Rmax, 0,  FALSE)
    rain_observed_values = c(rain_observed_values, p)
  }
  
  # writes observation values
  for (i in 1:stations_number){
    LocalUpsertQuantValue(QobsCodes[i], observed_date, flow_observed_values[i], mylog)
    LocalUpsertQuantValue(PobsCodes[i], observed_date, rain_observed_values[i], mylog)
  }
  
  # writes forecaste values and risk indicators
  stationsAlertValues = c()
  for (i in 1:stations_number){
    # forecast values
    indexes = which(((1:(forecast_number*stations_number))-i)%%stations_number == 0)
    Qvalues = flow_forecast_values[indexes]
    Pvalues = rain_forecast_values[indexes]
    for (t in 1:forecast_number){
      LocalUpsertQuantForecastValue(QprevCodes[i], now + 1, forecast_dates[t], Qvalues[t], mylog)
      LocalUpsertQuantForecastValue(PprevCodes[i], now + 1, forecast_dates[t], Pvalues[t], mylog)
    }
    # risk indicator
    qmax= max(Qvalues)
    LocalUpsertQuantValue(QmaxCodes[i], now, qmax, mylog)
    alert_index = which(alerts_limits*Qmax[i] > qmax)[1]
    LocalUpsertQualValue(alertCodes[i], now, alerts_names[alert_index], mylog)
    stationsAlertValues=c(stationsAlertValues, alerts_names[alert_index])
  }
  
  # writes risk indicator for reaches
  for (i in 1:length(reaches)){
    limits = rivLimits[[i]]
    alert = alerts_names[max(sapply(limits, FUN=function(x){which(alerts_names == stationsAlertValues[which(stations == x)[1]])[1]}))]
    LocalUpsertQualValue(rivAlertCodes[i], now, alert, mylog)
  }
  
}



###########################################################################################################################################
# GENERATEUR POUR LA PAGE URBAINE                                                                                                         #
###########################################################################################################################################

#' @export
urbanFloodingGenerator = function()
{
  
  # LOG INITIALIZATION
  mylog = function(message, level = 1){
    if (level == 1){
      Local_Log(message)
    } else if (level == 2) {
      Local_Warning(message)
    } else if (level == 3) {
      Local_Error(message)
    } else {
      Local_Warning("Level log unknown :" + level)
      Local_Warning(message)
    }
  }
  Local_logs_initialisation()
  
  # CONSTANTS
  initial_date = as.POSIXct("2019-04-16T00:00:00Z")
  now = as.POSIXlt(Sys.time(),"GMT")
  now$min = 0
  now$sec = 0
  now = as.POSIXct(now)
  
  # ###################################################################################################################################
  # CAN BE MODIFIED
  # ###################################################################################################################################
  
  forecast_duration_in_hours = 10  # forecast depth 
  forecast_timestep_in_hours = 1   # forecast timestep (1 forecast each forecast_timestep_in_hours)
  foreacast_periode_in_hours = 24  # the forecast is the same each foreacast_periode_in_hours
  Pmax = 100
  Pbase = 0
  
  alerts_names=c("green", "yellow", "orange", "red")
  alerts_limits=c(25, 60, 85,1000)
  
  stations = c("pluvio231", "Synop30", "pluvio74")
  quartiers = c("AERO", "MAKE", "STAD", "POTO", "BACO", "OUEN", "MOUN", "TALA", "MFIL")
  calcul = list(c(0.33, 0.67,0), c(0, 0.05, 0.95), c(0, 0.5, 0.5), c(0.4, 0.6, 0), c(0, 0.25, 0.75), c(1, 0, 0), c(0.5, 0.5, 0), c(1,0 ,0), c(0.5,0.5,0))
  
  obsSuffixe = "Pvirt"
  prevSuffixe = "Pprev"
  alertSuffixe = "alert"
  
  # ###################################################################################################################################
  
  # variables initilization
  forecast_dates = c()
  rain_forecast_values = c()
  alert_forecast_values = c()
  flow_observed_values = c()
  rain_observed_values = c()
  forecast_number = forecast_duration_in_hours / forecast_timestep_in_hours
  stations_number = length(stations)
  urban_number = length(quartiers)
  
  # loop over all forecast time
  prev = list(c(), c(), c())
  for (i in 1:forecast_number){
    
    # forecast date
    t = now + (i-1) * forecast_timestep_in_hours * 3600
    forecast_dates = c(forecast_dates, t)
    
    # forecast values
    for (j in 1:stations_number){
      # rainfall forecast
      q = forecastCalculation(t, initial_date, foreacast_periode_in_hours, j/stations_number, Pmax, Pbase,  TRUE)
      prev[[j]] = c(prev[[j]], q)
    }
  }
  forecast_dates = as.POSIXct(forecast_dates, origin="1970-01-01")
  
  # observations
  obs=c()
  observed_date = as.POSIXct(now - 3600) 
  for (j in 1:stations_number){
    # rainfall calculation
    p = forecastCalculation(observed_date, initial_date, foreacast_periode_in_hours, j/stations_number, Pmax, Pbase,  FALSE)
    rain_observed_values = c(rain_observed_values, p)
    obs=c(obs, p)
  }
  
  
  # writes observation values
  for (i in 1:stations_number){
    LocalUpsertQuantValue(paste(stations[i], obsSuffixe, sep=""), observed_date, rain_observed_values[i], mylog)
  }
  for (j in 1:urban_number){
    p = sum(calcul[[j]] * obs)
    LocalUpsertQuantValue(paste(quartiers[j], obsSuffixe, sep=""), observed_date, p, mylog)
  }
  
  # writes forecaste values and risk indicators
  stationsAlertValues = c()
  
  for (i in 1:stations_number){
    # forecast values
    for (t in 1:forecast_number){
      LocalUpsertQuantForecastValue(paste(stations[i], prevSuffixe, sep=""), now, forecast_dates[t], prev[[i]][t], mylog)
    }
    # risk indicator
    pmax= max(prev[[i]])
    alert_index = which(alerts_limits > pmax)[1]
    LocalUpsertQualValue(paste(stations[i], alertSuffixe, sep=""), now, alerts_names[alert_index], mylog)
  }
  for (i in 1:urban_number){
    urbprev= c()
    for (t in 1:forecast_number){
      pr=0
      for (j in 1:stations_number){
        pr = pr + prev[[j]][t] * calcul[[i]][j]
      }
      urbprev=c(urbprev, pr)
    }
    for (t in 1:forecast_number){
      LocalUpsertQuantForecastValue(paste(quartiers[i], prevSuffixe, sep=""), now, forecast_dates[t], urbprev[t], mylog)
    }
    pmax= max(urbprev)
    alert_index = which(alerts_limits > pmax)[1]
    LocalUpsertQualValue(paste(quartiers[i], alertSuffixe, sep=""), now, alerts_names[alert_index], mylog)
  }
}




###########################################################################################################################################
# GENERATEUR POUR LA PAGE HYDROELEEC                                                                                                      #
###########################################################################################################################################

#' @export
hydroElecGenerator = function(){
  
  # LOG INITIALIZATION
  mylog = function(message, level = 1){
    if (level == 1){
      Local_Log(message)
    } else if (level == 2) {
      Local_Warning(message)
    } else if (level == 3) {
      Local_Error(message)
    } else {
      Local_Warning("Level log unknown :" + level)
      Local_Warning(message)
    }
  }
  Local_logs_initialisation()
  
  # CONSTANTS
  initial_date = as.POSIXct("2019-04-16T00:00:00Z")
  now = as.POSIXct(Sys.time(),"GMT")
  now = as.POSIXct(as.Date(now))
  
  # ###################################################################################################################################
  # CAN BE MODIFIED
  # ###################################################################################################################################
  
  forecast_duration_in_hours = 24 * 10  # forecast depth 
  forecast_timestep_in_hours = 1 *24   # forecast timestep (1 forecast each forecast_timestep_in_hours)
  foreacast_periode_in_hours = 24 * 24  # the forecast is the same each foreacast_periode_in_hours
  Emax = c(1)
  Ebase = c(0)
  
  alerts_names=c("green", "yellow", "orange", "red")
  alerts_limits=c(25, 60, 85,1000)
  
  stations = c("barrage02")
  
  obsSuffixe = "Evirt"
  prevSuffixe = "Eprev"
  alertSuffixe = "alert"
  
  # ###################################################################################################################################
  
  # variables initilization
  forecast_dates = c()
  flow_forecast_values = c()
  rain_forecast_values = c()
  alert_forecast_values = c()
  flow_observed_values = c()
  rain_observed_values = c()
  forecast_number = forecast_duration_in_hours / forecast_timestep_in_hours
  stations_number = length(stations)
  
  # FORECAST
  for (i in 1:forecast_number){
    # forecast date
    t = now + (i-1) * forecast_timestep_in_hours * 3600
    forecast_dates = c(forecast_dates, t)
    
    # forecast values
    for (j in 1:stations_number){
      # flow forecast
      q = forecastCalculation(t, initial_date, foreacast_periode_in_hours, j/stations_number,Emax[j], Ebase[j],  TRUE)
      q =  2+ ((q*2)-1)
      flow_forecast_values = c(flow_forecast_values, q)
    }
  }
  forecast_dates = as.POSIXct(forecast_dates, origin="1970-01-01")
  
  
  # OBSERVATION
  observed_date = as.POSIXct(now - 86400) 
  for (j in 1:stations_number){
    # flow calculation
    q = forecastCalculation(observed_date, initial_date, foreacast_periode_in_hours, j/stations_number,Emax[j], Ebase[j],  FALSE)
    q = 2 + ((q*2)-1)
    flow_observed_values = c(flow_observed_values, q)
  }
  
  
  
  #WRITE
  # writes observation values
  for (i in 1:stations_number){
    LocalUpsertQuantValue(paste0(stations[i], obsSuffixe), observed_date, flow_observed_values[i], mylog)
  }
  # writes forecaste values and risk indicators
  stationsAlertValues = c()
  for (i in 1:stations_number){
    # forecast values
    indexes = which(((1:(forecast_number*stations_number))-i)%%stations_number == 0)
    Qvalues = flow_forecast_values[indexes]
    for (t in 1:forecast_number){
      LocalUpsertQuantForecastValue(paste0(stations[i], prevSuffixe), now + 1, forecast_dates[t], Qvalues[t], mylog)
    }
  }
  
  
}